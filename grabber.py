import datetime
import os
import random
import shutil
import string
import sys
import threading
import time

import httplib2
from PIL import Image, UnidentifiedImageError

INVALID = [0, 503]

THREADS = []


def scrape_pictures(thread, in_valid_display):
    while True:
        # url = 'http://img.prntscr.com/img?url=http://i.imgur.com/'
        url = 'https://i.imgur.com/'
        length = random.choice((5, 6))
        url += ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(length))
        url += '.jpg'

        file_name = 'images/' + url.rsplit('/', 1)[-1]
        path_cache = 'images/cache/.cache' + thread
        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S.%f')

        h = httplib2.Http(path_cache)
        response, content = h.request(url)
        out = open(file_name, 'wb')
        out.write(content)
        out.close()

        try:
            img = Image.open(file_name)
            img.verify()
            valid_image = True
        except UnidentifiedImageError:
            valid_image = False

        file_size = os.path.getsize(file_name)
        if valid_image and file_size not in INVALID:
            is_valid = "Valid"
        else:
            is_valid = "Invalid"
            os.remove(file_name)
            if not in_valid_display:
                continue

        print(st + "\t" + is_valid + "\t\tThread: " + thread + "\t" + url)
        out = open("imgurGrabber.log.csv", 'a')
        out.write(st + ";" + is_valid + ";Thread " + thread + ";" + url + "\n")
        out.close()

        time.sleep(.5)


def delete_contents(path):
    try:
        shutil.rmtree(path)
        print(f"Все файлы и папки в {path} успешно удалены.")
    except Exception as e:
        print(f"Ошибка при удалении файлов и папок в {path}: {e}")


def start(thread_amount: int):
    clear_images = input("Очистить папку images? [Y/n]: ")
    if clear_images.lower() != 'n':
        out = open("imgurGrabber.log.csv", 'w')
        out.write("Date;Status;Thread;URL\n")
        out.close()
        delete_contents("images")

    in_valid_display = False
    in_valid_display_input = input("Вывести невалидные файлы? [Y/n]: ")
    if in_valid_display_input.lower() != 'n':
        in_valid_display = True

    print("Scraping imgur images...")
    for thread_number in range(1, thread_amount + 1):
        THREADS.append(
            threading.Thread(target=scrape_pictures, args=(str(thread_number), in_valid_display), daemon=True))
        THREADS[thread_number - 1].start()
        print(f'Successfully started {thread_number} threads.')

    while True:
        time.sleep(1)


def error_threads_number():
    sys.exit("Вы ввели некорректное количество потоков.")


if __name__ == '__main__':
    user_input = input("Количество потоков: ")

    try:
        number = int(user_input)

        if number <= 0:
            error_threads_number()
        else:
            start(number)
    except ValueError:
        error_threads_number()
